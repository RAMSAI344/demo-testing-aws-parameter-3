import boto3
import base64

if __name__ == '__main__':
    session = boto3.session.Session()

    kms = session.client('kms')

    key_id = 'd1829fb5-77ba-4c57-9f65-6d6cb0ff2bae'
    stuff = kms.encrypt(KeyId=key_id, Plaintext='deploymentBucket1')
    binary_encrypted = stuff[u'CiphertextBlob']
    encrypted_password = base64.b64encode(binary_encrypted)
    print(encrypted_password.decode())